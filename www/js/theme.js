/**
 * Created by Squirrel on 14.08.15.
 */

var animate = false

jQuery(document).ready(function() {
    show_hide_footer_menu();

    jQuery('.footer-nav-head').click(function() {
        if (jQuery(window).width() < 768) {
            jQuery(this).parent().find('ul').toggle(300);
        }
    });

});

jQuery(window).resize(function(){
    show_hide_footer_menu();
});

function show_hide_footer_menu(){
    if (jQuery(window).width() < 768) {
        jQuery('.footer-col ul').hide();
    }

    if (jQuery(window).width() > 767) {
        jQuery('.footer-col ul').show();
    }
}